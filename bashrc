#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias tmux='tmux -2'
alias grep='grep --color'

# Prompt integration
# Gentoo version of prompt integration
PS1='\[\033[01;32m\]\u@\h\[\033[01;34m\] \w\[\033[01;34m\]\$\[\033[00m\] '

# Command colors
git config --global color.branch auto
git config --global color.diff auto
git config --global color.status auto

# ctrl - capslock
#/usr/bin/setxkbmap -option "ctrl:nocaps"

#THIS MUST BE AT THE END OF THE FILE FOR GVM TO WORK!!!
[[ -s "/home/mgdelacroix/.gvm/bin/gvm-init.sh" ]] && source "/home/mgdelacroix/.gvm/bin/gvm-init.sh"
