#!/bin/bash

## TODO
# Modificar el output de la batería cuando el ordenador está cargado y enchufado
#  -- OUTPUT DE EJEMPLO --
#  Battery 0: Unknown, 99%
#  Battery 0: Charging, 100%,  until charged
#  --
# Incluir la conexión a una red WiFi o la existencia de conexión por LAN

while true; do
	batt=$(acpi -b)

	case $batt in
	*Discharging*)
		batt=${batt#* * * }
		p=${batt%%, *}
		batt=${batt#*, }
		t=${batt% *}
		xsetroot -name "v$p [$t] - $(date +%R)"
		;;
	*)
		batt=${batt#* * * }
		p=${batt%%, *}
		batt=${batt#*, }
		t=${batt% * *}
		xsetroot -name "^$p [$t] - $(date +%R)"
		;;
	esac

	sleep 30
done &
