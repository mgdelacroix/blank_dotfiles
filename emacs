(load-theme 'wombat)

; Toolbar and menubar
(tool-bar-mode -1)
(menu-bar-mode -1)

; Tabs
(setq-default tab-width 2)
(setq-default indent-tab-mode nil)

; Gmail account. Receive mail
(setq gnus-select-method '(nnimap "gmail"
(nnimap-address "imap.gmail.com")
(nnimap-server-port 993)
(nnimap-stream ssl)))

(setq user-mail-address "mgdelacroix@gmail.com")
(setq user-full-name "Miguel de la Cruz")

; File association
(setq auto-mode-alist (cons '("\\.groovy$" . java-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.gsp$" . html-mode) auto-mode-alist))
