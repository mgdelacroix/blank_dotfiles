" Bundles declaration
Bundle 'scrooloose/nerdtree'
Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-surround'
Bundle 'kien/ctrlp.vim'
Bundle 'tpope/vim-surround'
Bundle 'vim-scripts/ZoomWin'
Bundle 'plasticboy/vim-markdown'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'rstacruz/sparkup'
Bundle 'majutsushi/tagbar'
Bundle 'vim-scripts/Gundo'
Bundle 'vim-scripts/vimwiki'
Bundle 'msanders/snipmate.vim'
Bundle 'godlygeek/tabular'
Bundle 'scrooloose/nerdcommenter'
Bundle 'airblade/vim-gitgutter'
Bundle 'flazz/vim-colorschemes'

" Bundles configuration

" CtrlP
set wildignore+=*.class

" VimWiki
let g:vimwiki_list = [{'path': '~/Dropbox/wiki', 'path_html': '~/Dropbox/wiki_html', 'auto_export': 1}]
