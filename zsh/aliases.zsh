alias tmux='tmux -2'
alias gsh='groovysh'

alias gwd='g wd'
alias gwdc='g wdc'
alias gl='g gl'

alias -g G='| grep'
alias gnri='grep -nri'
