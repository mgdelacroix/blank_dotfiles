# editor
export EDITOR=vim

# browser
export BROWSER=firefox

# java jdk
export JAVA_HOME=/opt/jdk1.7.0_21

# java opts
export JAVA_OPTS=-Xmx8192M

# path
function add_to_path
{
    for extra_path in $@
    do
        PATH=$PATH:$extra_path
    done
}

#GEMS_PATH=
#add_to_path "$GEMS_PATH"

export PATH

# functions
function modernize
{
    # system
    sudo pacman -Syu --noconfirm

    # ruby gems
    gem update

    # gvm
    gvm selfupdate

    # oh my zsh
    upgrade_oh_my_zsh
}
