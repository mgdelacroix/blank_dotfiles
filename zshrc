ZSH=$HOME/.zsh

for config_file in $ZSH/*.zsh
do
    source $config_file
done

[[ -s "/home/mgdelacroix/.gvm/bin/gvm-init.sh" ]] && source "/home/mgdelacroix/.gvm/bin/gvm-init.sh"
